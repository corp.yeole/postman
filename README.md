# Postman
In this project I have tried to cover both Rest & SOAP APIs with the respective methods. I have used the processes and methods of Postman to automate the API executions and validations. I have also used the features of static data driven testing using CSV and JSON file. For generating user friendly reports and enabling the integration with CI & CD tools I have used Newman command line along with HTML & HTML Extra report.
 

## Getting started
Anyone who wants to use this frameworks can clone and implement their respective scenarios. Softwares needed:
1. For development: Postman (latest version preferred) (Download link: https://www.postman.com/downloads/) 
2. For execution: Node.JS, Newman. 
(Download links Node.js: https://nodejs.org/en/download/package-manager , 
 newman: https://learning.postman.com/docs/collections/using-newman-cli/installing-running-newman/)

## Newman Commands
1. Install Newman: npm install -g newman
2. Install HTML reporter: install -g newman-reporter-html
3. Install HTMLextra reporter: install -g newman-reporter-htmlextra


## Git commands to clone and pull
Git clone: https://gitlab.com/testing.yeole/postman.git
Git  pull: https://gitlab.com/testing.yeole/postman.git


## Postman version
I have used v11.1.0


## Project status
Completed

